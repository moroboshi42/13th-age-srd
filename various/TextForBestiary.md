#Text formatted in markdown for manual insertion

# Death-Plague Orc

When a creature contracts a death-plague orc disease, roll a d4 for the disease type and a d6 for that disease’s symptoms after the battle. Until cured, at the start of each day (after each full heal-up) the PC makes a Constitution check (including any appropriate background) to determine the severity of the disease for that day, or if the PC is cured.
Full bed rest under the care of a healer reduces severity roll from a d6 to a d4 (so a severity check result of 18 would be 1d4 + 2 not 1d6 + 2). A PC under the care of and experience healer or someone who knows healing magic gains a +10 bonus to the Constitution check. At the GM’s option, special ingredients or healing items, found via quest, can give additional bonuses to the check.

## Disease Type (d4)
- 1:  Filthy Fever (d6 roll for symptoms that day)
- 2:  Chatter Pox (d6 roll for symptoms that day)
- 3:  Slug Scourge (d6 roll for symptoms that day)
- 4:  Red Fever (d6 roll for symptoms that day)

### Filthy Fever Symptoms (d6)
- 1:  Slight temperature.
- 2–3:    The runs. You gain 1 less recovery than normal after a full heal-up due to fluid loss.
- 4+: You have a hacking cough that sounds like insane laughter and are losing fluids. You gain 2 fewer recoveries than normal after a full heal-up and can’t remain silent.

### Chatter Pox Symptoms (d6)
- 1:  Unsightly and painful sores, occasional shivers.
- 2–3:    Running sores, shivering and chattering teeth. You take a –3 penalty to social skill checks and to attack rolls with spells.
- 4+: Infected sores, fever dreams. Your constitution is weakened. After each full heal-up, roll 1d4 + 3 to determine your starting maximum recoveries for that day. Each time you cast a spell that isn’t at-will, there’s a 25% chance it fails (but you don’t expend the spell).

### Slug Scourge Symptoms (d6)
- 1:  Extreme appetite, nausea.
- 2–3:    Vomiting up slugs. Whenever you make a non-combat Charisma check, roll 2d20 and take the lower result.
- 4+: The slugs are inside your lungs! After each quick rest, you lose 25% of your maximum hit points. If you drop to 0 hp this way, you enter into a coma until the next day.

### Red Fever Symptoms (d6)
- 1:  Scarlet stripes on the eyeballs, itching, buzzing in ears.
- 2–3:    Blurred vision. You take a –3 penalty to ranged attacks.
- 4+: Fever, rage, and confusion. Whenever you roll a natural odd attack roll, you are confused until the end of your next turn. Ranged attacks have a 25% chance of accidentally targeting an ally (check before making the attack roll).

### Disease Severity 
(Con check each day; result affects d6 symptoms roll)
- 1–14:   d6 + 3
- 15–20:  d6 + 2
- 20–24:  d6 + 1
- 25–29:  d6 + 0
- 30+:    Cured


## Dire Animals

Dire animals are bigger (large-sized) versions of normal animals. Dire animals usually have at least one dire feature, determined randomly using the table below.\n\n|d6 |Dire Feature      | Description\n|---|------------------|------------\n|1 | Armor plates      | Add +2 to the dire animal's AC, and add +1 to its PD.\n|2 | Spiky bits        | Whenever an enemy hits the dire animal with a melee attack, deal damage equal to twice the animal's level to that attacker.\n|3 | Carnage           | The dire animal's attacks that miss deal damage equal to its level. When staggered, its missed attacks deal damage equal to double its level.\n|4 | Poison            | The dire animal's main attack also deals 5 ongoing poison damage per tier (5 ongoing poison at levels 1–4, 10 at 5–7, etc.).\n|5 | Dire regeneration | When the escalation die is even, this animal heals damage equal to triple its level at the start of its turn.\n|6 | Fury              | While staggered, the dire animal gains a +2 attack bonus and deals +4 damage, but at the end of each of its turns it takes 2d6 damage.


## Parasistic Dybbuk 
Special Dybbuk Abilities: 

Choose ONE\n\n-  **C: Visage of innocence: +11 vs. MD (one nearby enemy)—** The target can’t attack the dybbuk until the end of its next turn.\n-  **C: Horrific visage: +11 vs. MD (1d3 nearby enemies)—** The target is weakened (save ends).\n-  **C: Puppet strings: +11 vs. MD—** The target is confused (save ends).\n-  *Scratching nails:* When the target is engaged with the dybbuk at the start of its turn, it takes 20 damage.\n-  Warped flesh: The dybbuk heals 30 hp.


## Gelatinous Tethaedron

*Instinctive actions:* Gelatinous creatures have no brains, sometimes they just do things. When the escalation die is odd, instead of making an attack or moving, roll a d4 to see what the tetrahedron does. If an option is not viable (you roll a 1 but there is no engaged enemy), reroll until you get a valid option.\n\n1. **C: Fling: +9 vs. PD (one engaged enemy)—** 14 damage, and the target pops free from the ooze and must roll an easy save (6+); on a failure, it loses its next move action\n2. **C: Fling +9 vs. PD (one nearby enemy not engaged with the ooze)—** 28 damage, and the target is flung somewhere nearby and must roll an easy save (6+); on a failure, it loses its next move action\n3. **C: Fling +9 vs. PD (one far away enemy)—** 42 damage, and the target is flung somewhere far away and must roll an easy save (6+); on a failure, it loses its next move action\n4.  As a standard action the ooze quickly moves around the battlefield, oozing over 1d3 nearby enemies. Those enemies become engaged with the ooze and stuck (save ends).<br />*First failed save:* The target is engulfed instead of stuck.


## Gelatinous Cubahedron (aka Cube)
*Instinctive actions:* 

Gelatinous creatures have no brains, sometimes they just do things. When the escalation die is odd, instead of making an attack or moving, roll a d6 to see what the cubahedron does. If an option is not viable (you roll a 5 but there is no engulfed enemy), reroll until you get a valid option.\n\n1. The cubahedron jiggles in place. Each nearby enemy takes 5 acid damage. Each creature engulfed by the cube takes a –4 penalty to its saves until the end of its next turn.\n2. The cubahedron moves as a quick action. If the cube ends its move engaged with enemies, each of those enemies must roll a save; on a failure, the cubahedron grabs them (but they’re not engulfed).\n3. The cubahedron spits an engulfed creature into the air above it and makes a *shlup’n’schlorp* attack against that creature with a +5 attack bonus. Then the creature is engulfed again.\n4. The cubahedron flattens itself slightly and crawls up a wall and possibly across the ceiling. The cube falls at the end of its turn. Each creature engulfed by the cube takes 30 damage, and it makes a *Gel drop* attack against enemies below it.<br/>**Gel drop +10 vs. PD (1d3 nearby enemies)—** 15 damage, and the cube engulfs the target if it’s smaller than the cube\n5. The cubahedron moves one creature engulfed by it to the surface. The target gains a +4 bonus to attempts to escape the cube, but each time the cube is targeted by an attack, the engulfed creature must roll a save; on a failure, it becomes the target of the attack instead.\n6. The cubahedron spits out each enemy engulfed by it with great force in different.

## Gelatinous Octhaedron
*Instinctive actions:* 

Gelatinous creatures have no brains, sometimes they just do things. When the escalation die is odd, instead of making an attack or moving, roll a d8 to see what the octahedron does. If an option is not viable (you roll a 3 but there are no nearby targets), reroll until you get a valid option.\n\n1. **C: Acid splash +11 vs. PD (1d3 nearby or far away enemies in a group)—** 8 ongoing acid damage<br />Each failed save: Acid destroys one of the target’s non-magical items. The target takes a cumulative –1 attack penalty until the end of the battle (boots fall apart, shield straps snap, etc.).<br />*Miss:* 5 ongoing acid damage.\n2. The octahedron grows an orifice that gushes out a liquid glue that floods the area. Each enemy engaged with the octahedron is stuck (hard save ends, 16+). Each nearby enemy not engaged with the octahedron is stuck (save ends). Each far away enemy is stuck (easy save ends, 6+). The glue has no effect on creatures who are flying or that have some way of avoiding it.\n3. The octahedron squirts a slick slime that targets 1d3 nearby enemies. Until the end of the battle, each target must roll an easy save each time it moves; on a failure, it’s hampered until the end of its next turn.\n4. The octahedron makes a *Fire gout* attack as it splashes out a gel that ignites in the air and sticks to skin and clothing.<br />**C: Fire gout +11 vs. PD (1d3 nearby or far away enemies in a group)—** 8 ongoing fire damage.<br />Each failed save: The ongoing damage for all enemies hit by the attack increases by 1.\n5. The octahedron sweats acid. Each enemy engaged with the octahedron must roll a normal save; on a failure, it takes 40 acid damage.<br />On a success it takes 20 acid damage.\n6. **C: Acid jet +11 vs. PD (one nearby or far away enemy)—** 45 acid damage<br />*Miss:* 8 ongoing acid damage.\n7. **C: Acid geyser +11 vs. PD (1d3 nearby or far away enemies)—** 30 acid damage, and the octahedron is propelled uncontrollably about the area, passing next to each of its enemies. Unlike normal, each enemy can make an opportunity attack against the ooze as it moves this way, but the ooze can make a *Glomp* attack against those who do as a free action as it passes. (Tell the PCs that the ooze will get a counter-attack.)<br />**Glomp +9 vs. PD—** 10 acid damage, and the cube engulfs the target if it’s smaller than the cube\n8. The octahedron splits into two tetrahedrons, and each one can act this turn (roll a d4 for the instinctive actions of each one). Divide the octahedron’s current hit points equally between the two new creatures.

## Gelatinous Dodecahedron

*Instinctive actions:* 

Gelatinous creatures have no brains, sometimes they just do things. When the escalation die is odd, instead of making an attack or moving, roll a d12 to see what the dodecahedron does. If an option is not available (you roll a 1 but there are no nearby enemies), reroll until you get a valid option.\n\n1. The dodecahedron makes a squash attack against 1d4 nearby enemies as it rolls and shlorps around the area. Any enemies already engulfed by the dodecahedron take 10 thunder damage.<br />**C: Squash +10 vs. PD (1d4 nearby enemies)—** 20 damage, and the target is stunned (easy save ends, 6+)\n2. The dodecahedron throws out whip-like tendrils and makes a *Sudden orifice* attack against each enemy engaged with it. Then it pulls each nearby enemy next to it and engages that creature.<br />**Sudden orifice +12 vs. PD (each enemy engaged with it)—** The dodecahedron engulfs the target if it’s smaller than the dodecahedron\n3. Hundreds of finger-size slimes slither out from the interior of the dodecahedron and begin worming their way across the bodies of each of its enemies in the battle. Until the end of the battle, when a non-ooze creature takes any damage besides ongoing acid damage, it also takes 10 acid damage.\n4. The dodecahedron bounces and thrums. Each enemy engaged with it or engulfed by it must roll a save; on a failure, that enemy takes 30 thunder damage. On a success, it takes 15 thunder damage and pops free from the dodecahedron.\n5. The dodecahedron hunkers down and produces acidic spikes. It gains a +4 bonus to all defenses until the end of the battle, and each enemy who makes a melee attack against it with a non-magical weapon must roll a save; on a failure the weapon dissolves. When a creature’s weapon dissolves, that creature is hampered and weakened (save ends both). Magical weapons lose their bonuses to hit and damage until the end of the battle instead of being dissolved (but the owner is still hampered and weakened). They can be “restored” after the battle by taking a few minutes during a rest to re-attune them.\n6. The dodecahedron moves, rolling around the battlefield, then makes a *shlup’n’schlorp* attack at the end of its movement with a +5 bonus. If the attack misses, instead of making a spasms attack the dodecahedron rolls about the area again and makes a second *shlup’n’schlorp* attack with a +10 bonus. If the second attack misses, it doesn’t get a spasms attack and the dodecahedron’s turn ends.\n7. The dodecahedron makes a *spasms* attack. If it misses with either attack roll, after the attacks, it can make a *stretch and engulf* attack as a free action.<br />**C: Stretch and engulf +12 vs. PD (one nearby or far away enemy)—** 30 acid damage, and the dodecahedron engulfs the target if it’s smaller than the dodecahedron\n8. **C: Pseudopod slaps +10 vs. AC (one nearby enemy)—** 25 thunder damage<br />*Natural odd hit:* The target pops free from the dodecahedron and is knocked far away, and the ooze makes the attack again against a different nearby enemy as a free action.*Natural even hit:* The dodecahedron engulfs the target if it’s smaller than the dodecahedron.\n9. Each enemy engulfed by the dodecahedron takes 40 acid damage, and the dodecahedron heals 40 hp for each enemy it has engulfed.\n10. The dodecahedron splits into an octahedron and a tetrahedron, and each one can act this turn (roll a d8 and d4 for the instinctive actions of each one). Divide the dodecahedron’s current hit points into thirds, and give one third to the tetrahedron and two thirds to the octahedron.\n11. The dodecahedron splits into three tetrahedrons, and each one can act this turn (roll a d4 for the instinctive actions of each one). Divide the dodecahedron’s current hit points equally between the three new creatures.\n12. The dodecahedron splits into two cubahedrons, and each one can act this turn (roll a d6 for the instinctive actions of each one). Divide the dodecahedron’s current hit points equally between the two new creatures.


## Kobold Skyclaw

**C: Alchemical flask +6 vs. PD (one nearby enemy or a far away enemy at –2 atk)—** 3 damage, and roll a d4 for the effect of the flask’s contents.


1. *Distilled ankheg spit:* The target takes 3 ongoing acid damage.\n2. *Reconstituted remorhaz lymph:* The target takes 3 ongoing fire damage.\n3. *Essence of giant spider web:* The target is stuck (save ends).\n4. *Kobold blasting powder:* There’s a loud bang and the battlefield is obscured with thick smoke. Each non-kobold creature takes a –2 attack penalty during its next turn.



*Erratic flight:* 

The kobold flings itself from a trebuchet or ignites an unstable alchemical propellant and lands among its enemies. Each time the kobold uses a move action to fly, roll a d20.\n| roll | Effect\n|------|-------\n|1     |It crashes and explodes (see mook).\n|2–15: |It lands safely.\n|16+:  |It stays aloft and can keep flying.


## Prismatic Ogre Mage

**R: Prismatic blast +15 vs. PD (1d3 nearby or far away enemies in a group)—** 


Roll a d8 against each hit target to determine the type of ray and effec t\n\n1. *Red:* 75 fire damage; *Miss:* 40 fire damage.\n2. *Orange:* 50 damage, and the target is confused (save ends).\n3. *Yellow:* 40 ongoing poison damage.\n4. *Green:* The target loses a recovery, and the prismatic ogre mage heals 70 hp.\n5. *Blue:* 60 lightning damage, and make a secondary attack: +15 vs. PD (one enemy near the target)— 20 lightning damage.\n6. *Cyan:* The target is stunned (save ends).\n7. *Purple:* The target is dazed and confused (save ends both). When the target saves, it takes 40 psychic damage.\n8. *Magenta:* The target is transported into the future. Remove that creature from play, returning it to the battle in (or near) its previous location at the end of its next turn. No time seems to pass for the target while it’s gone.


## Orc Battle Screamer

*Orcish Instruments: 

__Choose ONE__:\n\n- **R: Skull drum +7 vs. MD—** 8 damage, and as a free action, one nearby orc ally can move or make a basic attack (doesn’t trigger special abilities)\n- **R: Bone flute +7 vs. MD—** 8 damage, and one nearby orc ally deals +1d6 damage on a hit during its next turn\n- **R: War bagpipes—** 1d3 nearby or far away enemies that can hear the bagpipes must immediately roll a normal save; on a failure, the target is hampered until the end of its next turn

## Pixie warrior
*[Special trigger] Madness +8 vs. MD— *\n\nThe target is maddened until the end of the pixie’s next turn. The first thing the creature does on its turn is use a standard action to make a display of power that has no practical benefit. The GM chooses the attack, spell, or other power, which should be the most powerful one the character has, preferably a daily. The target expends the power in a great show of prowess, aimed for display rather than effect. Wizards cast their fireballs into the sky, monks demonstrate flawless form while shadow-boxing, and clerics bless the very stones underfoot instead of allies.\n\nWhen the madness effect gets old, substitute any of the following effects, by choice or at random:\n\n1. You love pixies and hate those who threaten them. The target is confused until the end of the pixie’s next turn.\n2. Brains knocked loose. The target takes 4 psychic damage each time it takes an action (save ends; yes that could be 3 times per round or more).\n3. Reality bender. The target takes 5 ongoing psychic damage. Each time the target fails the save, the ongoing damage increases by 5, cumulative.\n4. Massive attack on the unconscious mind. The target chooses one: either let the onslaught slam its mind (take 15 psychic damage), or steel its mind against the onslaught (stunned until the end of the pixie’s next turn).\n5. Compulsion to dance. The target takes 5 ongoing psychic damage (no save) until it spends a standard action to dance maniacally. Dancing ends all such ongoing damage, even if the target has been hit multiple times with this effect.\n6. Pacifism. The target can’t attack until the end of the pixie’s next turn.

## Basilisk

* Last Gasp Failed Save Effects: *

- **Green Basilisk:**\n    - *Failed save:* The target takes 25 poison damage as its blood turns to poison.\n    - *Fourth failed save:* The target’s body liquefies and they die.\n- **Red Basilisk:**\n    - *Failed save:* The target takes 15 fire damage, and each of the target’s nearby allies takes 5 fire damage as the target’s blood boils under their skin and sprays outward in gouts of liquid fire.\n    - *Fourth failed save:* The target’s body ignites as their flaming skull shoots upward on a jet of burning blood, killing them.\n- **Black Basilisk:**\n    - *Failed save:* The target’s blood begins to coagulate and harden, and dust puffs out of their lungs through their mouth.\n    - *Fourth failed save:* The target’s body turns to stone and they die.\n- **White Basilisk:**\n    - *Failed save:* The target begins to evaporate. The target must make an immediate normal save (11+) or it loses a chunk of flesh or a limb, whatever is dramatic but will allow the creature to survive (perhaps with minuses until healed).\n    - *Fourth failed save:* The target’s body explodes into a fine red mist and they die.

## Iconic Chimera - Chimerical change

*Chimerical change:* 

At the start of battle each PC rolls one icon relationship die of their choice and one die chosen from their relationships by the GM. A roll of 6 means the chimera has shifted in a manner that gives it a chimerical flaw (F) that the adventurers can exploit. A roll of a 5 indicates that the chimera gains both a flaw and a benefit associated with that icon. A result of 1 or 2 means that the chimera gains the icon’s chimerical benefit (B) without the flaw. Actual benefits and flaws are up to the GM, and should reflect the icons with which the players have relationships.\n\n- ***(B)*** *Bearded spell-eating head:* When a spell attack misses the chimera, it has no miss effect and the chimera heals 40 hp.\n- ***(F)*** *Vulnerability, melee:* The crit range of melee attacks against the chimera expands by 2.\n- ***(B)*** *Skin of indomitable faces:* When the chimera saves against a condition, it becomes immune to that condition until the end of the battle.\n- ***(F)*** *Vulnerability, spells:* The crit range of spell attacks against the chimera expands by 2.\n- ***(B)*** *Flaming oil glands:* When a creature is engaged with the chimera at the start of its turn, it takes 20 fire damage.\n- ***(F)*** *Overconfident predator:* Each time the chimera scores a critical hit, it takes a cumulative –2 penalty to all defenses.\n- ***(B)*** *Stone-studded dermis:* Any damage dice that roll maximum damage against the chimera count as 1s.\n- ***(F)*** *Stubborn beast:* The chimera won’t willingly disengage from a creature engaged with it.\n- ***(B)*** *Springing stag legs:* When the escalation die is odd, the chimera gains an additional standard action that turn.\n- ***(F)*** *Overly gracile:* The chimera’s form is unusually frail, and any damage dice that roll 1s against the chimera count as 2s.\n- ***(B)*** *Mane of scorpion tails:* The chimera has a fear aura: Enemies engaged with the chimera who are below 48 hit points are dazed and can’t use the escalation die.\n- ***(F)*** *Golden fleece:* The chimera has transformed itself into a form that’s gem encrusted with golden fur. When slain it will yield 1d6 x 100 gp per adventurer in gems and precious metals. At the GM’s say-so, knowledge of this bounty may grant particularly mercenary characters a +1 attack bonus against the chimera once it’s staggered.\n- ***(B)*** *Inferno belly:* When the chimera makes a fiery breath attack, it can make another fiery breath attack that turn as a quick action.\n- ***(F)*** *Flammable blood:* When the chimera rolls a natural odd miss with fiery breath, it takes 20 fire damage.\n- ***(B)*** *Wild heart:* The chimera is constantly evolving and changing. When an enemy rolls a natural 1–5 on an attack roll against it, the chimera adds the escalation die to its attack rolls until the end of its next turn.\n- ***(F)*** *Vulnerability, poison:* The crit range of poison attacks against the chimera expands by 2.\n- ***(B)*** *Bone plates:* When an enemy rolls a natural even miss with a melee attack against the chimera, the weapon temporarily loses all its bonuses and powers and the wielder is weakened (save ends both).\n- ***(F)*** *Vulnerability, holy:* The crit range of holy attacks against the chimera expands by 2.\n- ***(B)*** *Mighty tusks:* As a standard action, the chimera can make a tusk charge attack. It can’t make any other attacks the same turn.\nTusk charge +20 vs. AC—100 damage\nLimited use: Once the chimera has damaged an enemy with this attack, it can’t use tusk charge again until it drops an enemy to 0 hp or lower.\n- ***(F)*** *Too many horns:* The horns and tusks of the chimera lock together at inopportune moments. When it rolls a natural 1–5 with an attack against multiple targets, it loses any further attacks that turn.\n- ***(B)*** *Eagle wings:* The chimera can fly like an eagle, swooping in and out of the battle.\n- ***(F)*** *Vulnerability, negative energy:* The crit range of negative energy attacks against the chimera expands by 2.\n- ***(B)*** *Poison fangs:* When the chimera rolls a natural 1–5 with an attack and misses, the target takes 20 ongoing poison damage.\n- ***(F)*** *Bickering heads:* The first time each turn the chimera hits with an attack, it rerolls the attack and takes the lower roll.\n- ***(B)*** *Draconic sinews:* While not staggered, the chimera rolls 2d20 with its melee attacks and uses the higher roll.\n- ***(F)*** *Too many wings:* Whenever the chimera moves, it rolls an easy save (6+). On a failure, it provokes opportunity attacks from each nearby enemy.

## Iconic Chimera - Tusk charge

*Tusk charge table:*
- ***(F)*** *Too many horns:* The horns and tusks of the chimera lock together at inopportune moments. When it rolls a natural 1–5 with an attack against multiple targets, it loses any further attacks that turn.\n- ***(B)*** *Eagle wings:* The chimera can fly like an eagle, swooping in and out of the battle.\n- ***(F)*** *Vulnerability, negative energy:* The crit range of negative energy attacks against the chimera expands by 2.\n- ***(B)*** *Poison fangs:* When the chimera rolls a natural 1–5 with an attack and misses, the target takes 20 ongoing poison damage.\n- ***(F)*** *Bickering heads:* The first time each turn the chimera hits with an attack, it rerolls the attack and takes the lower roll.\n- ***(B)*** *Draconic sinews:* While not staggered, the chimera rolls 2d20 with its melee attacks and uses the higher roll.\n- ***(F)*** *Too many wings:* Whenever the chimera moves, it rolls an easy save (6+). On a failure, it provokes opportunity attacks from each nearby enemy.

## Stirgeling

- *Regular stirgeling:* The target takes 3 damage, and 3 ongoing damage.\n- *Archer stirgeling:* The target takes 6 damage.\n- *Cobbler stirgeling:* The target takes 3 damage and is stuck until end of its next turn.

## Wendigo Spirit

- *1–2:* The target takes 10 damage as it bites itself.\n- *3–4:* The target makes a basic attack against its nearest or most vulnerable ally (moving to that target if necessary).\n- *5–6:* The target attacks the wendigo if it can; otherwise, it does nothing.

## Sahuagin Glow Priest

- *(1–2) (Hymn of hate):* One random nearby conscious enemy takes 4d6 negative energy damage.\n- *(3–4) (Curse of despair):* Each target that was hit takes a –2 penalty (non-cumulative) to all saves until the end of the battle.\n- *(5–6) (Scream of victory):* Until the start of the glowpriest’s next turn, each sahuagin and demon in the battle adds the escalation die to its attacks and the PCs don’t.\n- *(7–8) (Word of refuge):* Remove the glowpriest from play. At the start of its next turn, return it to play nearby its original location. It gains a +4 bonus to all defenses until the end of its next turn after it returns to play.

## Ogre Mage Knight

- *Natural even hit:* 25 damage, and the ogre mage knight can use lightning pulse as a free action.\n- *Natural odd hit:* 20 damage, and the ogre mage knight can use voice of thunder as a free action.\n- *Natural even miss:* 10 damage, and the ogre mage knight can teleport to any nearby location it can see before using magi’s lightning chain as a free action.\n- *Natural odd miss:* The ogre mage knight can use cone of cold as a free action.


*Trollish regeneration 15:* While an ogre mage is damaged, its uncanny flesh heals 15 hit points at the start of the ogre mage’s turn. It can regenerate five times per battle. If it heals to its maximum hit points, then that use of regeneration doesn’t count against the five-use limit.\nWhen the ogre mage is hit by an attack that deals fire or acid damage, it loses one use of its regeneration, and it can’t regenerate during its next turn.\nDropping an ogre mage to 0 hp doesn’t kill it if it has any uses of regeneration left.


##Half-Orc Legionnaire

- *Natural even hit:* The half-orc legionnaire gains a +2 bonus to all defenses until the start of its next turn.\n- *Natural odd hit:* The target takes +1d6 damage.\n- *Natural even miss:* 4 damage.\n- *Natural odd miss:* If the legionnaire’s next melee attack is a natural even hit, it becomes a critical hit instead!


## Death-Plague Orc

When a creature contracts a death-plague orc disease, roll a d4 for the disease type and a d6 for that disease’s symptoms after the battle. Until cured, at the start of each day (after each full heal-up) the PC makes a Constitution check (including any appropriate background) to determine the severity of the disease for that day, or if the PC is cured.

Full bed rest under the care of a healer reduces severity roll from a d6 to a d4 (so a severity check result of 18 would be 1d4 + 2 not 1d6 + 2). A PC under the care of and experience healer or someone who knows healing magic gains a +10 bonus to the Constitution check. At the GM’s option, special ingredients or healing items, found via quest, can give additional bonuses to the check.

Disease Type (d4)
- 1:  Filthy Fever (d6 roll for symptoms that day)
- 2:  Chatter Pox (d6 roll for symptoms that day)
- 3:  Slug Scourge (d6 roll for symptoms that day)
- 4:  Red Fever (d6 roll for symptoms that day)

Filthy Fever Symptoms (d6)
- 1:  Slight temperature.
- 2–3:    The runs. You gain 1 less recovery than normal after a full heal-up due to fluid loss.
- 4+: You have a hacking cough that sounds like insane laughter and are losing fluids. You gain 2 fewer recoveries than normal after a full heal-up and can’t remain silent.

Chatter Pox Symptoms (d6)
- 1:  Unsightly and painful sores, occasional shivers.
- 2–3:    Running sores, shivering and chattering teeth. You take a –3 penalty to social skill checks and to attack rolls with spells.
- 4+: Infected sores, fever dreams. Your constitution is weakened. After each full heal-up, roll 1d4 + 3 to determine your starting maximum recoveries for that day. Each time you cast a spell that isn’t at-will, there’s a 25% chance it fails (but you don’t expend the spell).

Slug Scourge Symptoms (d6)
- 1:  Extreme appetite, nausea.
- 2–3:    Vomiting up slugs. Whenever you make a non-combat Charisma check, roll 2d20 and take the lower result.
- 4+: The slugs are inside your lungs! After each quick rest, you lose 25% of your maximum hit points. If you drop to 0 hp this way, you enter into a coma until the next day.

Red Fever Symptoms (d6)
- 1:  Scarlet stripes on the eyeballs, itching, buzzing in ears.
- 2–3:    Blurred vision. You take a –3 penalty to ranged attacks.
- 4+: Fever, rage, and confusion. Whenever you roll a natural odd attack roll, you are confused until the end of your next turn. Ranged attacks have a 25% chance of accidentally targeting an ally (check before making the attack roll).

Disease Severity (Con check each day; result affects d6 symptoms roll)
- 1–14:   d6 + 3
- 15–20:  d6 + 2
- 20–24:  d6 + 1
- 25–29:  d6 + 0
- 30+:    Cured


## Demonic Ogre

Demonic advantage: Roll a d6 to determine which demonic ability the ogre gains.\n\n1. The ogre gains resist energy 16+.\n2. Once per battle, the ogre can teleport anywhere it can see as a move action.\n3. The ogre’s attacks deal +1d8 damage, hit or miss.\n4. At the start of the ogre’s turn, each enemy engaged with it takes 2d6 negative energy damage.\n5. The ogre gains a +5 bonus to saves this battle.\n6. The ogre begins making demonic eruption rolls when the escalation die is 4+ instead of 6+.


## Deathly Warbanner

C: Banner magic—Roll 2d20 and use each roll (or a lower result of your choice) to determine one effect

Banner magic effect table:\n\n- 1–5: The warbanner’s bearer can make a basic attack with a +2 attack bonus as a free action.\n- 6–10: Each different enemy engaged with a creature in the warbanner’s band takes 1d10 psychic damage.\n- 11–15: If any mooks in the warbanner’s band have dropped this battle, return one of those mooks to the battle next to the warbanner. (If the warbanner has no mooks in its band, choose a lower result.)\n- 16–20: The warbanner can make a false rally attack as a free action.


## Feral Warbanner

Banner magic effect table:\n\n- 1–5: The warbanner’s bearer can make a basic attack with a +2 attack bonus as a free action.\n- 6–10: Each different enemy engaged with a creature in the warbanner’s band takes 1d6 psychic damage.\n- 11–15: If any mooks in the warbanner’s band have dropped this battle, return one of those mooks to the battle next to the warbanner. (If the warbanner has no mooks in its band, choose a lower result.)\n- 16–20: The warbanner can make a false rally attack as a free action.

## Zealous Warbanner

Banner Magic effect table:\n\n- 1–5: The warbanner’s bearer can make a basic attack with a +2 attack bonus as a free action.\n- 6–10: Each different enemy engaged with a creature in the warbanner’s band takes 1d8 psychic damage.\n- 11–15: Until the end of the battle, each ally in the warbanner’s band gains a +2 cumulative bonus to damage on hits with melee attacks.\n- 16–20: The warbanner can make a false rally attack as a free action.


## Drow Cavalry

Spider mount: 

Whenever the drow cavalry rolls a natural 1–10 on an attack roll, its spider mount acts independently, choosing one of the following options:\\n\\n-  **Bite—** The spider makes a bite attack.\\n-  **Jump & Scuttle—** The spider and its rider pop free from all enemies and can move somewhere nearby.\\n-  **Web—** The spider makes a web attack.


## Intellect Assassin

*Psychic duel:*

A psychic duel occurs when the intellect assassin hits with an insidious domination attack against a creature. At the start of the creature’s next turn, it must make an Intelligence skill check and can use a background that applies to psychic ability (if any). The result of this check determines that creature’s status until the start of its next turn. To maintain the psychic duel, the assassin must spend a move action each turn.

__Psychic Duel Result: Status__\n\n-  15 or less: The creature is confused until the end of its turn. It also can’t make opportunity attacks until the start of its next turn and takes a –2 penalty to its next psychic duel check.\n-  16–22: The creature is pressured—it takes a –2 penalty to attacks against any enemy except the assassin (dueling opponent).\n-  23–27: The creature steadies itself and can use a move action this turn to escape the psychic duel. If the creature chooses not to (or can’t) escape the duel, it gains a +2 bonus to its next psychic duel check.\n-  28+: As 23–27, but the creature takes advantage and gains a +4 bonus to its next psychic duel check instead of +2. In addition, the assassin takes a –2 penalty to attack rolls that target other creatures.


## Winter Beast

*Winter Beast Special Ability:** 

Choose ONE

- Armored polar bear: The target takes 10 extra damage, and if the target makes an opportunity attack against the polar bear before the start of the bear’s next turn, the bear can make a fang, claw, or tusk attack against the target as a free action.
- Giant walrus: The target takes 14 extra damage and is stuck (save ends; also ends if the walrus moves)
- Winter wolf: The target takes 14 extra damage, or 28 extra damage if another winter wolf is engaged with it.


## Warped Beast

One madness feature

At the start of each of the warped beast’s turns, roll a d6. The warped beast gains the corresponding ability until the start of its next turn.\n\n1. *Amorphous oozing form* The beast has resist damage 11+ to all damage.\n2. *Dimensional slide* Once during its turn, the warped beast can teleport anywhere nearby it can see as a move action. Each enemy engaged with it when it teleports is confused until the end of its next turn.\n3. *Fear aura* While engaged with the warped beast, enemies that have 24 hp or fewer are dazed (–4 attack) and do not add the escalation die to their attacks.\n4. *Gibbering mouths* When an enemy ends its turn engaged with the warped beast, it’s confused until the end of its next turn.\n5. *Many spontaneous limbs* When the warped beast makes a tentacle maw attack during its turn, roll a d4. That many additional limbs or tentacles spontaneously erupt from the creature and make an additional basic attack that turn (special abilities/effects don’t trigger on those extra attacks). Each of those attacks only deals half damage.\n6. *Warping touch* When the warped beast hits a creature with a tentacle maw attack, the target also takes 5 ongoing psychic damage and a –2 penalty to saves (save ends both).

# Trog

Trog stench: 

Trogs spray scents that stink so badly that other humanoids take penalties to all attacks, defenses, and saves when engaged with a troglodyte or when nearby three or more troglodytes. Non-humanoids aren’t affected.\n\nHumanoids affected by *trog stench* can make a normal save (with a penalty) at the end of each of their turns. If the save succeeds, the humanoid can ignore all *trog stench* for the rest of the battle.\n\nPenalties vary for different humanoid races:\n\n|Race|Save Penalty|\n|:---|:----------:|\n|Elves, gnolls, gnomes|-4\n|Humans, halflings, half-elves, Aasimar, tieflings, etc.| -3\n|Half-orcs, draconics|-2\n|Dwarves| -1\n|Steelforged| 0

# Werebeasts

Each werebeast can take one of three forms as a quick action once per round: a humanoid form, a beast form, or a hybrid form that combines the two. The hybrid form is the most powerful in combat, but the humanoid and beast forms are useful for various activities outside of combat as well.
The stats below work for the beast and hybrid forms. If a werebeast ends up fighting in humanoid form, subtract 2 from all its defenses and halve its normal attack damage.

The following powers are common for werebeasts.

Beast heart (all three forms): Animals of the werebeast’s type are intuitively on good terms with the were, even dire animals. If the werebeast betrays them, however, retaliation is certain and merciless.

Cursed bite (hybrid or animal form only): While the moon is full, the first time each battle a creature takes damage from a werebeast melee attack, it takes 10 extra damage (champion: 20 damage; epic: 40 damage). The malignant curse, transmitted through the attack, shocks the mortal flesh. Further attacks, even from weres of different types, don’t cause extra damage. Unless blessed, purged, or otherwise cured, the damaged creature will turn into a werebeast on the night of the next full moon. Heroes powerful enough to fight werebeasts can usually find benefactors or rituals to cure them, but there’s an exception to every rule. GM, if you’re unsure whether it’s a full moon, roll a d10. On a 1–3, the moon is full enough.

Resilient shifting (all three forms): As mentioned above, a werebeast can shift forms once per round as a quick action. When a werebeast shifts, it can roll a save against one save ends effect.

Nastier Specials for all werebeasts

The following ability is extremely nasty when combined with the bestial fury most werebeasts have. It’s possibly better used as a story option, a consequence of a problem with a skill check or an icon relationship, or a situation that should have been avoided and may require sacrifices to deal with:

Moon fury (hybrid form only): While fighting in moonlight, a werebeast gains a bonus to damage equal to its level (champion: double its level; epic: triple it).

# Demon 
Demons may or may not have a special demonic ability. Roll 1d10 for each non-mook normal-sized demon. If you roll less than or equal to the demon’s level, it has a random ability. Large or huge (or double- or triple-strength) demons automatically get one at least one random ability.\n\nWhen a demon has an ability, roll 1d6 or 1d8 (GM’s choice) on the table below to determine what it is.\n\nd6 or d8| Demonic Ability |Description\n-------|:----------------|:---------\n1|True seeing| The demon is immune to invisibility and ignores any illusions.\n2|Resist fire 18+| You’ll see that the demon resists fire the first time you use fire against it.\n3|Invisibility |The first time the demon is staggered in a battle, it becomes invisible until the end of its next turn.\n4|Resist energy 12+|The demon's resistance to all energy types puts a damper on enemy spellcasters, but at least the resistance is only 12+.\n5|Fear aura|Enemies engaged with the demon who are below its fear hit point threshold are dazed and can’t use the escalation die. See Fear.\n6|Teleport |1d3 times per battle, as a move action, the demon can teleport anywhere it can see nearby.\n7|Demonic speed|The demon can take an extra action each turn while the escalation die is 4+.\n8|Gate |Once per battle as a standard action, if the demon is staggered, it can summon a single demon ally at least two levels below its own level. The allied demon rolls initiative and does not appear on the battlefield until its turn starts.


# Devil

Apply to Skin Devil, Lemure, Hell Imp, Honey Devil, aka Slime Devil, Smoke Devil, Bearded Devil (Barbazu), Hooded Devil, Fury Devil (Erinyes), Bone Devil (Osyluth), Barbed Devil (Hamatula), Ice Devil (Gelugon), Horned Devil (Cornugon), Pit Fiend

Devils have other special abilities that can be picked randomly or purposely. Choose 0 to 2 abilities per devil. Some abilities may be conditional. For example, a devil may only get a special ability while a boss devil is commanding them.\n\n|d10|Devil Ability|Description\n|---|-------------|-----------|\n|1 |Alternative element| Change the resist fire 13+ ability to a different element, e.g. lightning or acid.\n|2 |Beast form |This rare special ability lets a devil pass unnoticed through populated areas, though with a tell-tale sign that it’s preternatural, such as a big cat that looks normal except for its forked tongue. Changing forms is a move action.\n|3  |Devilish resilience|The devil gains a +4 bonus to saves.\n|4  |Fear|The devil gains fear aura. Players with fewer hit points than double the devil's current hit points are dazed. They cannot use the escalation die.\n|5  |Final-gasp strike|When the devil drops to 0 hp, it stays up until its turn, at which point it can take one last standard action before dropping. The devil will drop without getting that last action if it takes enough damage to reduce its hit points to negative 25% of its starting hp.\n|6  |Humanoid form|This rare special ability lets the heroes meet a pit fiend over dinner and not know it until too late. Changing forms is usually a move action.\n|7  |Resist non-damage effects|The devil resists the effects (but not the damage) of attacks with a natural attack roll of 12 or less. Ongoing damage isn’t affected by this ability. If the attack deals energy damage that the devil is resistant to (like fire), that damage is halved as normal. This ability is tied to a devil’s energy resistance number, so if you increase that number for a devil with this ability, the non-damage range also increases.\n|8  |Summon other devils|This ability is better used as a conditional feature of a particular battle than as a standard special ability. On the first round of battle, a group of devils keens eerily, and everyone knows they’re summoning something, but no one knows exactly what, or when it will show up.\n|9   |True seeing, see invisible|Powerful devils might have the ability to see through simple ruses and illusions.\n|10  |Unearthly toughness|If a devil takes X damage or less, negate that damage. Bigger blows have full effect, but petty blows are beneath the devil’s notice. Set X at the devil’s level + 1. It will be immune to normal miss damage from enemies that are its level or lower. Or set X at 5% of its starting hit points to have the value scale up faster at higher levels.

# Drow

Drow Cruelty
Choose one of the following abilities for all non-mook drow in a battle to have:\n\n- *Poisoner:* When the drow scores a critical hit, it can forego damage to force the target to start making last gasp saves. On the fourth failure, the target falls unconscious (and can’t be woken normally) for 2d4 minutes.\n- *Take advantage of weakness:* Whenever an enemy nearby a drow rolls a save, it takes its level in damage.\n-  *Umbral caul:* As long as at least one drow uses a standard action each round to maintain the supernatural darkness, each non-drow creature in the battle becomes dazed (save ends) whenever it rolls a natural 1–5 with an attack roll.\n- *Venomous:* The first time each battle a drow hits with a weapon attack, the target also takes 5 ongoing poison damage.

Poisons and Potions
Drinking a potion, applying a poison, or using a substance requires only a quick action for a drow with one of these options. Characters who acquire any of these drow items must use a standard action to drink/apply them.\n\n- *Draft of Eschaton (potion):* When drunk it immediately purges the body of all poisons and toxins and ends any ongoing conditions. All the drinker’s saves (including last gasp saves and death saves) automatically succeed for the rest of the battle or for five minutes. Immediately after the battle, the drinker must make a DC 30 Constitution check or be completely debilitated and unable to travel far or to fight for a day.\n- *Dragon Apples:* Small ceramic spheres that explode. Make a basic ranged attack against a nearby enemy (or level + 5 for drow); on a hit, the target takes 10 ongoing fire damage.\n- *Midnight Tincture:* When this glass vial is broken, the nearby area is shrouded in clinging darkness. The darkness lasts 3 rounds. Each creature in the darkness except drow takes a –4 attack penalty. The tincture can be attached to an arrow or bolt.\n- *Potion of Spider Climbing:* For one battle or five minutes the drinker can climb on ceilings and walls as easily as it moves on the ground.\n-  *Spider Venom:* A creature hit by a melee weapon coated in the venom takes 5 ongoing poison damage when the attack roll is a natural 16+.\n- *Web Dust:* Made from desiccated spiders, this dust can hold a door shut or stick an object to a wall for five minutes. Champion-tier creatures can make a DC 20 skill check to rip through something held by web dust before it fully dissolves, and epic-tier creatures can rip through such objects in a round. Further applications increase the effectiveness: three applications lasts for fifteen minutes, requires 3 successful checks, and takes 3 rounds for epic-tier characters to get through.


# Metallic Dragon
**Breath Attack recharge**\n| Natural Attack Roll | Variable Waiting Period for Metallic Dragon’s At-Will Breath Attack |\n| :----: | --- |\n| 1-5  | The dragon can use its breath weapon again next turn.\n| 6-10 | The dragon must wait one turn before it can use its breath weapon.\n| 11+  | After waiting one turn, the dragon rolls a normal save at the start of its next turn. If it succeeds, it can use its breath weapon that turn. Otherwise, it keeps rolling a save at the start of each turn until it regains its breath weapon attack. Once it uses its breath again, use the attack roll again to determine its next breath weapon attack.

## Special abilities

| d20  | Dragon Ability |  Description\n| :--: |----------------| ------------\n| 1 | True seeing | The dragon is immune to invisibility and ignores illusions.\n| 2 | Whipping tail  | When an enemy engaged with the dragon rolls a natural 1 or 2 with an attack roll, the dragon can make an opportunity attack against that creature as a free action. The attack is set up by the dragon’s whipping tail but delivered by the dragon’s usual melee attack.\n| 3 | Tough hide | The dragon has a +1 bonus to AC.\n| 4 | Fortress mind  | The dragon has a +2 bonus to MD.\n| 5 | Nimble | The dragon has a +2 bonus to PD.\n| 6 | Murderous  | The crit range of the dragon’s melee attacks expands by 2.\n| 7 | Now I’m mad!   | The first time the dragon is staggered each battle, it uses its breath weapon attack as a free action that does not count against the normal uses of its breath.\n| 8 | Serious threat | Disengage checks against the dragon take a –5 penalty. When a creature fails to disengage from the dragon, it takes damage equal to double the dragon’s level.\n| 9 | PC-style racial power  | The dragon has one of the racial powers of a player character race. If the dragon’s story suggests a specific power, choose that. Otherwise, use the most common expression per color: brass (gnome, halfling); bronze (dark elf, dwarf, half-orc, human); copper (dwarf, human, wood elf); silver (forgeborn, wood elf); gold (high elf, holy one, human, wood elf).\n| 10 | Raw power  | Until it’s staggered, the dragon rolls 2d20 with its melee attacks and uses the higher roll.\n| 11 | Damage aura | When an enemy starts its turn engaged with the dragon, it takes damage equal to the dragon’s level (adventurer tier), double the level (champion tier), or triple the level (epic tier). The damage type is the same as the dragon’s breath weapon.\n| 12 | More breath | The dragon gains a +5 bonus to saves to regain its breath weapon.\n| 13 | Spellbreaker   | When the dragon hits a creature with an attack, one spell effect that creature created is negated (hard save ends, 16+). Once the creature saves, the effect returns.\n| 14 | Shake off  | At the start of each of its turns, the dragon can roll a save against one save ends effect on it.\n| 15 | Free to flee   | Like the PCs, the dragon can escape at any time by giving up what amounts for it to a campaign loss. If the PCs are the ones to benefit from this, they should experience this as a major victory. But the dragon is still out there.\n| 16 | Air supremacy  | The crit range of the dragon’s attacks against flying creatures expands by an amount equal to the escalation die.\n| 17 | Denial | If a PC or other creature uses an attack against the dragon that is related to an icon that the dragon also has a relationship with, the dragon gains a +5 bonus to all defenses against that attack, and it only takes damage from the attack without suffering any of its effects.\n| 18 | Survivor   | Each time the dragon drops to 0 hit points, it can roll a save. The first save in a battle is easy (6+), the second normal, the third and subsequent saves are hard (16+). If it succeeds, the dragon stays conscious and takes no damage from the attack or effect that would have dropped it.\n| 19 | Humanoid form  | The dragon is capable of shapechanging into a humanoid form, usually of a warrior or spellcaster appropriate to its nature and usually not obviously draconic, registering as a normal human or elf or whatever. This ability is best used for long-term dragon characters that make it worth the GM’s time to create a double- or triple-strength humanoid monster to represent the shapechanged form. The dragon has the PC-style racial power of their humanoid form, but only while in shapechanged form. Shapechanging is a move action.\n| 20 | Some unique thing  | The dragon has an entirely unique characteristic, something akin to a player character’s one unique thing except that the dragon’s version may be relevant to combat. GM, if you don’t feel like making something up, choose an ability from the list above.

#Chromatic dragon
## Special abilities

Dragons may have random abilities. For GMs who are uncertain whether a medium dragon should have a random ability, roll a d8. Otherwise, roll a d12. If the roll is less than or equal to the dragon’s level, it gets a random ability according to the table below. Some huge dragons have two abilities. If you want to give the dragon a chance of having one of the abilities that has campaign implications, add +2 to the roll.\n\n| d8 or d12  | Dragon Ability |  Description\n| :--: |----------------| ------------\n| 1 |  True seeing | The dragon is immune to invisibility and ignores any illusions.\n| 2 | Whipping tail  | When an enemy engaged with the dragon rolls a natural 1 or 2 with an attack roll, the dragon can make an opportunity attack against that creature as a free action. The attack is set up by the dragon’s whipping tail but delivered by the dragon’s usual melee attack.\n| 3 | Tough Hide | The dragon has a +1 bonus to AC.\n| 4 | Twisted Mind   | The dragon has a +2 bonus to MD.\n| 5 | Nimble | The dragon has a +2 bonus to PD.\n| 6 | No vulnerability   | Unlike other dragons of its color, this dragon has no vulnerability. The PCs will figure that out the first time they try to use its supposed vulnerability against it.\n| 7 | Now I’m mad!   | The first time the dragon is staggered each battle, it uses its breath weapon attack as a free action that does not count against the normal uses of its breath.\n| 8 | Serious threat | Disengage checks against the dragon take a –5 penalty. When a creature fails to disengage from the dragon, it takes damage equal to double the dragon’s level.\n| 9 | PC-style racial power  | The dragon has one of the racial powers of a player character race. The most common expressions per color are: white (halfling); black (halfling, half-orc, human, wood elf); green (dwarf, dark elf); blue (high elf, half-orc); red (half-orc, human, wood elf).\n| 10 | Raw power  | Until it is staggered, the dragon rolls 2d20 with its melee attacks and uses the higher roll.\n|11  | Damage aura | When an enemy starts its turn engaged with the dragon, it takes damage equal to the dragon’s level (adventurer tier), double the level (champion tier), or triple the level (epic tier). The damage type is the same as the dragon’s breath weapon.\n|12  | More breath | The dragon can use its intermittent breath 1d4 more times each battle. If its breath weapon isn’t intermittent (white and green dragons), the dragon gains the extra uses anyway, making it more dangerous than lesser specimens of its color.\n| 13 | Humanoid form  | The dragon is capable of shapechanging into a humanoid form, usually of a warrior or spellcaster appropriate to its nature and usually not obviously draconic, registering as a normal humanoid. The dragon has the PC-style racial power of their humanoid form, but only while in shapechanged form.\n| 14 | Some Unique Thing  | The dragon has an entirely unique characteristic, something akin to a player character’s one unique thing except that the dragon’s version may be relevant to combat. GM, if you don’t feel like making something up, choose an ability from the list above.
