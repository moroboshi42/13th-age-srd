# 13th-age-srd
*13th Age* Monster compendium for [Foundry Virtual Table Top](https://foundryvtt.com/). This is a compendium of SRD material for the *13th Age* roleplaying game. Currently this contains the monsters from *Core*, *Bestiary 1* and *13 True Ways* published by Pelgrane Press at [The Archmage Engine – 13th Age SRD](https://site.pelgranepress.com/index.php/the-archmage-engine-13th-age-srd/). Version 3.0 of 24 October 2013.

## Required
To use this compendium you must have installed the  [*13th Age* system](https://gitlab.com/asacolips-projects/foundry-mods/archmage) module.

## Installation
- Start up Foundry and click "Install Module" in the "Add-On Modules" tab.
- Paste the link: `https://gitlab.com/moroboshi42/13th-age-srd/-/raw/master/module.json`
- Click "Install" and it should appear in your modules list.
- Launch a *13th Age* world, then go to the settings tab and enable the module in module settings.
- The Bestiary should be available in the Compendium tab as *13th Age Monster SRD*

