# History

- 0.1.0 First version imported from [The Archmage Engine – 13th Age SRD](https://site.pelgranepress.com/index.php/the-archmage-engine-13th-age-srd/)
  - rollabe power (for demon, dragon, eccc) inserted in traits. To be moved to vtt foundry table when will be possible
  - common power (like fly for bat or dragon) integraded in every monster of that type
  - Resistance and Vulnerability copied in dedicated field. Left in trait only if they are inusual
- 0.2.0 Clean up: 
   - moved *Limited use* and other attack info in field description
   - Dire Bat: missing data (AC, PD, MD, HP) on srd text copied from Bestiary 1, p. 14
    -  Elder Couatl: missing data (AC, PD, MD, HP) on srd text copied from Bestiary 1, p. 53
    -  Smoke Devil: missing data (AC, PD, MD, HP) on srd text copied from True Ways, p. 172
    -  Drow Darkbolt: missing data (AC, PD, MD, HP) on srd text copied from Bestiary 1, p. 59